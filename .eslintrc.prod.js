module.exports = {
  extends: ['airbnb-base'],
  ignorePatterns: ['.eslintrc.js', 'webpack.config.js',],
  env: {
    browser: true,
    node: true,
  },
  globals: {
    PRODUCTION: 'readonly',
  },
  rules: {},
};
