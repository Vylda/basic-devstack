# Devstack - javascript pro pokročilé
Základní devstack založený na
- Webpack 5
- Babel
- PostCSS

## Co to umí
- transpilace moderního JS pro IE 11
  - async/await, promise, spreads atd…
  - pouze při buildu produkční verze
- minifikace obrázků
- obrázky jako moduly (`import Logo from ''./logo.png';`)
- podpora lokálních písem
- autoprefixer pro CSS
- minifikace (css, html, js, obrázky) pro produkci
- podpora posledních dvou verzí známých browserů, které mají v ČR zastoupení ve více než 0,5 % a IE 11 pro desktop

## Instalace
```bash
git clone git@gitlab.com:Vylda/basic-devstack.git
cd basic-devstack
npm install
```

## Použití

### Struktura složek
#### dist
Sestavené soubory aplikace (css, html, js, atd.). V případě spuštění webového vývojového serveru je prázdná.
#### src
Zdrojové soubory Javascriptu. ve složce `css` jsou zdrojové soubory CSS, ve složce `images` pak obrázky pro import.
#### static
Statické soubory (např. ikony), které se samy nakopírují bez změny do složky dist při sestavování.

### Vývoj

spuštění vývojového serveru s automatickým sestavením vývojové verze (bez transpilace pro IE, bez minifikace, source mapy) a reloadem webové stránky po uložení souboru:
```bash
npm start
```

automatické sestavení vývojové verze po uložení souboru:
```bash
npm run watch
```

sestavení vývojové verze:
```bash
npm run dev
```

### Sestavení produkční verze
```bash
npm run build
```
Pokud není třeba **IE11**, stačí změnit následující soubory:
- *.browserslistrc*: odstranit řádek 3 `IE 11`
- *webpack.config.prod.js*: změnit řádek 12 `target: ['web', 'es5'],` na `target: 'web',`
