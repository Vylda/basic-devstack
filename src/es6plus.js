const block = document.createElement('div');
block.id = 'grid';

const dom = {
  p1: document.createElement('article'),
  p2: document.createElement('article'),
  p3: document.createElement('article'),
};

block.appendChild(dom.p1);
block.appendChild(dom.p2);
block.appendChild(dom.p3);

const fillP1 = () => {
  const header = document.createElement('h3');
  header.textContent = 'Array.from';

  const range = (start, stop, step) => Array.from(
    {
      length: (stop - start) / step + 1,
    },
    (_, i) => start + i * step,
  );
  const array = range('A'.charCodeAt(0), 'Z'.charCodeAt(0), 1).map((x) => String.fromCharCode(x));

  const paragraph = document.createElement('p');
  paragraph.textContent = array.join(', ');

  dom.p1.appendChild(header);
  dom.p1.appendChild(paragraph);
};

const fillP2 = () => {
  const header = document.createElement('h3');
  header.textContent = 'Spread object';
  const paragraph = document.createElement('p');

  const first = {
    first: 'první třída',
    second: 'druhá třída',
    third: 'třetí třída',
    fourth: 'čtvrtá třída',
    fifth: 'pátá třída',
  };

  const second = {
    sixth: 'šestá třída',
    seventh: 'sedmá třída',
    eighth: 'osmá třída',
    ninth: 'devátá třída',
  };

  const classes = { ...first, ...second };
  const keys = Object.keys(classes);
  const pairs = keys.map((key) => `${key}: ${classes[key]}`);

  paragraph.textContent = pairs.join(', ');

  dom.p2.appendChild(header);
  dom.p2.appendChild(paragraph);
};

const getString = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve('Tento text byl získán za 2,5 s pomocí Promise.');
  }, 2500);
});

const fillP3 = async () => {
  const header = document.createElement('h3');
  header.textContent = 'Async / await';
  const paragraph = document.createElement('p');
  paragraph.textContent = 'čekám na data…';
  dom.p3.appendChild(header);
  dom.p3.appendChild(paragraph);

  const data = await getString();
  paragraph.textContent = data;
  const appendix = document.createElement('p');
  appendix.textContent = 'Tento text byl vložen hlavní funkcí až po vykonání Promise.';
  dom.p3.appendChild(appendix);
};

fillP1();
fillP2();
fillP3();

export default block;
