import Logo from './images/webpack.svg';

const logoContainer = document.createElement('div');
logoContainer.className = 'logo';
const image = new Image();
image.src = Logo;
logoContainer.appendChild(image);

export default logoContainer;
