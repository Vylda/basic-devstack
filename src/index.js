import './css/index.css';
import { h1, h2 } from './header';
import envParagraph from './environment';
import es6Plus from './es6plus';
import logoContainer from './logo';

const dom = {
  root: document.querySelector('#app'),
};

dom.root.appendChild(h1);
dom.root.appendChild(h2);
dom.root.appendChild(envParagraph);
dom.root.appendChild(es6Plus);
dom.root.appendChild(logoContainer);
