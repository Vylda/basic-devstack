module.exports = {
  extends: ['airbnb-base'],
  ignorePatterns: [
    '.eslintrc.js',
    '.eslintrc.prod.js',
    'webpack.config.common.js',
    'webpack.config.dev.js',
    'webpack.config.prod.js',
    'postcss.config.js',
  ],
  env: {
    browser: true,
    node: true,
  },
  rules: {
    'no-unused-vars': 'warn',
    'no-debugger': 'warn',
  },
  globals: {
    PRODUCTION: 'readonly',
  }
};
